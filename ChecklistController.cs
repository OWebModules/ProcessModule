﻿using LogModule;
using LogModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ProcessModule.Factories;
using ProcessModule.Models;
using ProcessModule.Services;
using ProcessModule.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule
{
    public class ChecklistController : AppController
    {
        public string IdClassWorkingList
        {
            get
            {
                return Checklist.Config.LocalData.Class_Working_Lists.GUID;
            }
        }

        public clsOntologyItem LogStatePause
        {
            get
            {
                return Checklist.Config.LocalData.Object_Pause;
            }
        }

        public clsOntologyItem LogStateError
        {
            get
            {
                return Checklist.Config.LocalData.Object_Error;
            }
        }

        public clsOntologyItem LogStateSuccess
        {
            get
            {
                return Checklist.Config.LocalData.Object_Success;
            }
        }

        public List<AdditionalReportField> GetAdditionalReportFields()
        {
            var result = new List<AdditionalReportField>();

            var idLogEntry = new AdditionalReportField
            {
                Name = "IdLogEntry",
                Title = "IdLogEntry",
                Hidden = true,
                DataType = "String",
                First = false,
                Template = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("#= (IdLogEntry == null) ? '' : IdLogEntry #")),
                Width = "150px"
            };

            var nameLogEntry = new AdditionalReportField
            {
                Name = "NameLogEntry",
                Title = "Log Entry",
                Hidden = false,
                DataType = "String",
                First = false,
                Template = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("#= (NameLogEntry == null) ? '' : NameLogEntry #")),
                Width = "150px"
            };

            var idLogState = new AdditionalReportField
            {
                Name = "IdLogState",
                Title = "IdLogState",
                Hidden = true,
                DataType = "String",
                First = false,
                Template = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("#= (IdLogState == null) ? '' : IdLogState #")),
                Width = "150px"
            };

            var nameLogState = new AdditionalReportField
            {
                Name = "NameLogState",
                Title = "Log State",
                Hidden = false,
                DataType = "String",
                First = false,
                Template = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("#= (NameLogState == null) ? '' : NameLogState #")),
                Width = "150px"
            };

            var dateTimeStamp = new AdditionalReportField
            {
                Name = "DateTimeStamp",
                Title = "Edit Date",
                Hidden = false,
                DataType = "DateTime",
                First = false,
                Template = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("#= (DateTimeStamp == null) ? '' : kendo.toString(kendo.parseDate(DateTimeStamp, 'yyyy-MM-ddTHH:mm:ss'),'dd.MM.yyyy HH:mm:ss')#")),
                Width = "150px"
            };

            var sbButton = new StringBuilder();
            sbButton.AppendLine("<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example \">");
            sbButton.AppendLine("<button class='success-entry'><i class='fa fa-check' aria-hidden='true'></i></button>");
            sbButton.AppendLine("<button class='edit-entry'b><i class='fa fa-pencil aria-hidden='true'></i></button>");
            sbButton.AppendLine("<button class='error-entry'b><i class='fa fa-exclamation-circle aria-hidden='true'></i></button>");
            sbButton.AppendLine("</div>");

            var buttonField = new AdditionalReportField
            {
                Name = "buttonField",
                Title = "Actions",
                Hidden = false,
                DataType = "Bit",
                First = true,
                GroupCell = true,
                Template = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(sbButton.ToString())),
                Width = "123px"
            };

            result.Add(buttonField);
            result.Add(idLogEntry);
            result.Add(nameLogEntry);
            result.Add(idLogState);
            result.Add(nameLogState);
            result.Add(dateTimeStamp);

            return result;
        }

        public async Task<ResultItem<ChangeChecklistEntryStateResult>> ChangeChecklistEntryState(ChangeChecklistEntryStateRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<ChangeChecklistEntryStateResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ChangeChecklistEntryStateResult()
                };

                request.MessageOutput?.OutputInfo($"Validating request...");

                result.ResultState = ValidationController.ValidateChangeChecklistEntryStateRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Validated request.");

                var serviceAgent = new ServiceAgentElastic(Globals);

                var searchObjects = new List<clsOntologyItem>();

                request.MessageOutput?.OutputInfo($"Getting checklist-searches, logstate-searches and reference-searches from {request.ChecklistEntryStateItems.Count} request-entries...");
                foreach (var changeEntry in request.ChecklistEntryStateItems)
                {
                    if (!searchObjects.Any(obj => obj.GUID == changeEntry.IdChecklist))
                    {
                        searchObjects.Add(new clsOntologyItem
                        {
                            GUID = changeEntry.IdChecklist
                        });
                    }

                    if (!searchObjects.Any(obj => obj.GUID == changeEntry.IdLogState))
                    {
                        searchObjects.Add(new clsOntologyItem
                        {
                            GUID = changeEntry.IdLogState
                        });
                    }

                    if (!searchObjects.Any(obj => obj.GUID == changeEntry.IdReference))
                    {
                        searchObjects.Add(new clsOntologyItem
                        {
                            GUID = changeEntry.IdReference
                        });
                    }

                    if (!searchObjects.Any(obj => obj.GUID == changeEntry.IdUser))
                    {
                        searchObjects.Add(new clsOntologyItem
                        {
                            GUID = changeEntry.IdUser
                        });
                    }
                }

                request.MessageOutput?.OutputInfo($"Getting {searchObjects} objects...");
                var searchObjectResult = await serviceAgent.GetObjects(searchObjects);

                result.ResultState = searchObjectResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Having {searchObjectResult.Result} objects.");

                var logController = new LogController(Globals);
                var relationConfig = new clsRelationConfig(Globals);

                var relationsToSave = new List<clsObjectRel>();


                request.MessageOutput?.OutputInfo($"Saveing log-entries...");
                foreach (var entry in request.ChecklistEntryStateItems)
                {
                    var checklist = searchObjectResult.Result.FirstOrDefault(obj => obj.GUID == entry.IdChecklist);
                    if (checklist == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Checklist {entry.IdChecklist} could not be found!";
                        return result;
                    }

                    var logState = searchObjectResult.Result.FirstOrDefault(obj => obj.GUID == entry.IdLogState);
                    if (logState == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Logstate {entry.IdLogState} could not be found!";
                        return result;
                    }

                    var refItem = searchObjectResult.Result.FirstOrDefault(obj => obj.GUID == entry.IdReference);
                    if (refItem == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Reference {entry.IdReference} could not be found!";
                        return result;
                    }

                    var userItem = searchObjectResult.Result.FirstOrDefault(obj => obj.GUID == entry.IdUser);
                    if (userItem == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"User {entry.IdUser} could not be found!";
                        return result;
                    }

                    var name = $"{logState.Name}: {DateTime.Now}";
                    if (!string.IsNullOrEmpty(entry.Message))
                    {
                        name = entry.Message.Length > 255 ? entry.Message.Substring(0, 255) : entry.Message;
                    }
                    var logItem = new LogItem
                    {
                        IdUser = userItem.GUID,
                        NameUser = userItem.Name,
                        NameLogEntry = $"{logState.Name}: {DateTime.Now}",
                        IdLogState = logState.GUID,
                        NameLogState = logState.Name,
                        DateTimeStamp = DateTime.Now,
                        Message = entry.Message
                    };

                    var logItemResult = await logController.SaveLogItem(logItem, refItem, 1);
                    result.ResultState = logItemResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Logstate for reference {refItem.GUID} cannot be saved!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var relation = relationConfig.Rel_ObjectRelation(checklist, new clsOntologyItem
                    {
                        GUID = logItemResult.Result.IdLogEntry,
                        Name = logItemResult.Result.NameLogEntry,
                        GUID_Parent = logController.ClassLogEntry.GUID,
                        Type = Globals.Type_Object
                    }, Config.LocalData.RelationType_contains);

                    relationsToSave.Add(relation);
                }

                if (relationsToSave.Any())
                {
                    var saveRelResult = await serviceAgent.SaveRelations(relationsToSave);
                    result.ResultState = saveRelResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "The relations between checklists and logitems cannot be saved!";
                        return result;
                    }
                }

                request.MessageOutput?.OutputInfo($"Saved log-entries.");

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<Models.Checklist>> GetChecklist(GetChecklistRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<Models.Checklist>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new Models.Checklist()
                };

                var elasticAgent = new ServiceAgentElastic(Globals);
                request.MessageOutput?.OutputInfo("Validate Request...");

                result.ResultState = Validation.ValidationController.ValidateGetChecklistRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated Request.");

                request.MessageOutput?.OutputInfo("Get Checklist-Model...");
                var modelResult = await elasticAgent.GetChecklistModel(request);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Checklist-Model.");


                request.MessageOutput?.OutputInfo("Get Logentries...");
                var logController = new LogController(Globals);
                var logItemsResult = await logController.GetLogItems(modelResult.Result.WorkingList, true);
                result.ResultState = logItemsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Logentries.");

                modelResult.Result.LogItems = logItemsResult.Result;

                request.MessageOutput?.OutputInfo("Get additional Report-Fields...");

                modelResult.Result.AdditionalReportFields = GetAdditionalReportFields();

                request.MessageOutput?.OutputInfo("Have additional Report-Fields.");

                request.MessageOutput?.OutputInfo("Create Checklist...");
                var checklistFactory = new ChecklistFactory(Globals);
                var factoryResult = await checklistFactory.CreateChecklist(modelResult.Result, Globals);

                result.ResultState = factoryResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Checklist.");

                result.Result = factoryResult.Result;

                return result;
            });

            return taskResult;
        }

        public ChecklistController(Globals globals) : base(globals)
        {
        }
    }
}
