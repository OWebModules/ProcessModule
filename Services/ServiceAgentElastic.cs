﻿using LogModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ProcessModule.Models;
using ProcessModule.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string idItem, string type)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
               {
                   var result = new ResultItem<clsOntologyItem>
                   {
                       ResultState = globals.LState_Success.Clone()
                   };

                   var searchItem = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID = idItem
                       }
                   };

                   var dbReader = new OntologyModDBConnector(globals);
                   if (type == globals.Type_Class)
                   {
                       result.ResultState = dbReader.GetDataClasses(searchItem);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the class!";
                           return result;
                       }
                       result.Result = dbReader.Classes1.FirstOrDefault();
                       if (result.Result == null)
                       {
                           result.ResultState = globals.LState_Nothing.Clone();
                           result.ResultState.Additional1 = "The requested class cannot be found!";
                           return result;
                       }
                   }
                   else if (type == globals.Type_AttributeType)
                   {
                       result.ResultState = dbReader.GetDataAttributeType(searchItem);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the attributetype!";
                           return result;
                       }
                       result.Result = dbReader.AttributeTypes.FirstOrDefault();
                       if (result.Result == null)
                       {
                           result.ResultState = globals.LState_Nothing.Clone();
                           result.ResultState.Additional1 = "The requested attributetype cannot be found!";
                           return result;
                       }
                   }
                   else if (type == globals.Type_RelationType)
                   {
                       result.ResultState = dbReader.GetDataRelationTypes(searchItem);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the relationtype!";
                           return result;
                       }
                       result.Result = dbReader.RelationTypes.FirstOrDefault();
                       if (result.Result == null)
                       {
                           result.ResultState = globals.LState_Nothing.Clone();
                           result.ResultState.Additional1 = "The requested relationtype cannot be found!";
                           return result;
                       }
                   }
                   else if (type == globals.Type_Object)
                   {
                       result.ResultState = dbReader.GetDataObjects(searchItem);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the object!";
                           return result;
                       }
                       result.Result = dbReader.Objects1.FirstOrDefault();
                       if (result.Result == null)
                       {
                           result.ResultState = globals.LState_Nothing.Clone();
                           result.ResultState.Additional1 = "The requested object cannot be found!";
                           return result;
                       }
                   }
                   else
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "The requested type is not valid!";
                       return result;
                   }

                   return result;
               });

            return taskResult;

        }

        public async Task<ResultItem<ProcessModuleConfigModel>> GetProcessModuleModel()
        {
            var taskResult = await Task.Run<ResultItem<ProcessModuleConfigModel>>(() =>
           {
               var result = new ResultItem<ProcessModuleConfigModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ProcessModuleConfigModel()
               };

               var searchBaseConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = Config.LocalData.Class_Process_Module.GUID
                   }
               };

               var dbReaderBaseConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderBaseConfig.GetDataObjects(searchBaseConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the BaseConfig!";
                   return result;
               }

               result.Result.BaseConfig = dbReaderBaseConfig.Objects1.SingleOrDefault();

               if (result.Result.BaseConfig == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Only one BaseConfig can be defined!";
                   return result;
               }

               var searchLanguage = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.BaseConfig.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Process_Module_is_described_by_Language.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Process_Module_is_described_by_Language.ID_Class_Right
                   }
               };

               var dbReaderLanguage = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLanguage.GetDataObjectRel(searchLanguage);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Language!";
                   return result;
               }

               result.Result.Language = dbReaderLanguage.ObjectRels.Select(rel => new clsOntologyItem { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other, Type = rel.Ontology }).SingleOrDefault();

               if (result.Result.Language == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Only one Language can be assigned!";
                   return result;
               }

               var searchMediaStorePath  = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.BaseConfig.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Process_Module_Mediastore_Path_Path.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Process_Module_Mediastore_Path_Path.ID_Class_Right
                   }
               };

               var dbReaderMediaStorePath = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderMediaStorePath.GetDataObjectRel(searchMediaStorePath);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Mediastore-Path!";
                   return result;
               }

               result.Result.MediaStorePath = dbReaderMediaStorePath.ObjectRels.Select(rel => new clsOntologyItem { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other, Type = rel.Ontology }).SingleOrDefault();

               if (result.Result.MediaStorePath == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Only one MediaStore-Path can be assigned!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetSubProcessesL1(clsOntologyItem parentProcess, string nameProcess = "")
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               List<clsObjectRel> searchSubProcesses = new List<clsObjectRel>();

               searchSubProcesses.Add(new clsObjectRel
               {
                   ID_Object = parentProcess.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Process_superordinate_Process.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Process_superordinate_Process.ID_Class_Right
               });

               var dbReaderSubProcesses = new OntologyModDBConnector(globals);
               result.ResultState = dbReaderSubProcesses.GetDataObjectRel(searchSubProcesses);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the subprocesses!";
                   return result;
               }

               if (nameProcess == string.Empty)
               {
                   result.Result = dbReaderSubProcesses.ObjectRels;
               }
               else
               {
                   result.Result = dbReaderSubProcesses.ObjectRels.Where(rel => rel.Name_Other == nameProcess).ToList();
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<ProcessReference>>> GetProcessRefeences(List<clsOntologyItem> processes)
        {
            var taskResult = await Task.Run<ResultItem<List<ProcessReference>>>(() =>
           {
               var result = new ResultItem<List<ProcessReference>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<ProcessReference>()
               };

               var searchProcessReferences = processes.Select(proc => new clsObjectRel
               {
                   ID_Object = proc.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Process_contains_Process_References.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Process_contains_Process_References.ID_Class_Right
               }).ToList();

               var processReferences = new List<clsObjectRel>();
               var dbReaderProcessReferences = new OntologyModDBConnector(globals);

               if (searchProcessReferences.Any())
               {
                   result.ResultState = dbReaderProcessReferences.GetDataObjectRel(searchProcessReferences);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Process-References!";
                       return result;
                   }

                   processReferences = dbReaderProcessReferences.ObjectRels;
               }

               var searchReferences = processReferences.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other
               }).ToList();

               var dbReaderReferences = new OntologyModDBConnector(globals);

               var references = new List<clsObjectRel>();

               if (searchReferences.Any())
               {
                   result.ResultState = dbReaderReferences.GetDataObjectRel(searchReferences);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the references of projectreferences!";
                       return result;
                   }

                   references = dbReaderReferences.ObjectRels;
               }

               result.Result = (from proc in processes
                                join procRef in processReferences on proc.GUID equals procRef.ID_Object
                                join reference in references on procRef.ID_Other equals reference.ID_Object
                                where reference.ID_Other != proc.GUID
                                select new ProcessReference { ProcessOrProcessLog = proc, ProcessReferences = procRef, References = reference }).ToList();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> GetProcessDescription(List<clsOntologyItem> processes)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectAtt>>>(() =>
           {
               var result = new ResultItem<List<clsObjectAtt>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectAtt>()
               };

               var searchDescription = processes.Select(proc => new clsObjectAtt
               {
                   ID_Object = proc.GUID,
                   ID_AttributeType = Config.LocalData.ClassAtt_Process_Description.ID_AttributeType
               }).ToList();

               var dbReaderDescription = new OntologyModDBConnector(globals);

               if (searchDescription.Any())
               {
                   result.ResultState = dbReaderDescription.GetDataObjectAtt(searchDescription);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the descriptions of processes!";
                       return result;
                   }

                   result.Result = dbReaderDescription.ObjAtts;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<ChecklistModel>> GetChecklistModel(GetChecklistRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ChecklistModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ChecklistModel()
                };

                var searchChecklistItem = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdChecklist
                    }
                };

                var dbReaderWorkingList = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderWorkingList.GetDataObjects(searchChecklistItem);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Working-List!";
                    return result;
                }

                result.Result.WorkingList = dbReaderWorkingList.Objects1.FirstOrDefault();
                result.ResultState = ValidationController.ValidateChecklistModel(result.Result, globals, nameof(ChecklistModel.WorkingList));

                var searchLogState = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.WorkingList.GUID,
                        ID_RelationType = Checklist.Config.LocalData.ClassRel_Working_Lists_is_in_State_Logstate.ID_RelationType,
                        ID_Parent_Other = Checklist.Config.LocalData.ClassRel_Working_Lists_is_in_State_Logstate.ID_Class_Right
                    }
                };

                var dbReaderLogState = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderLogState.GetDataObjectRel(searchLogState);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading the Logstate!";
                    return result;
                }

                result.Result.WorkingListToLogState = dbReaderLogState.ObjectRels.FirstOrDefault();

                var searchReport = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.WorkingList.GUID,
                        ID_RelationType = Checklist.Config.LocalData.ClassRel_Working_Lists_belongs_to_Reports.ID_RelationType,
                        ID_Parent_Other = Checklist.Config.LocalData.ClassRel_Working_Lists_belongs_to_Reports.ID_Class_Right
                    }
                };

                var dbReaderReport = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReport.GetDataObjectRel(searchReport);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading the Report!";
                    return result;
                }

                result.Result.WorkingListToReport = dbReaderReport.ObjectRels.FirstOrDefault();

                result.ResultState = ValidationController.ValidateChecklistModel(result.Result, globals, nameof(ChecklistModel.WorkingListToReport));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReportField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.WorkingList.GUID,
                        ID_RelationType = Checklist.Config.LocalData.ClassRel_Working_Lists_belongs_to_Report_Field.ID_RelationType,
                        ID_Parent_Other = Checklist.Config.LocalData.ClassRel_Working_Lists_belongs_to_Report_Field.ID_Class_Right
                    }
                };

                var dbReaderReportField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportField.GetDataObjectRel(searchReportField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading the Report-Field!";
                    return result;
                }

                result.Result.WorkingListToReportField = dbReaderReportField.ObjectRels.FirstOrDefault();

                result.ResultState = ValidationController.ValidateChecklistModel(result.Result, globals, nameof(ChecklistModel.WorkingListToReportField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchResources = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.WorkingList.GUID,
                        ID_RelationType = Checklist.Config.LocalData.ClassRel_Working_Lists_belonging_Resource.ID_RelationType
                    }
                };

                var dbReaderResources = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderResources.GetDataObjectRel(searchResources);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Resources!";
                    return result;
                }

                result.Result.WorkingListToResource = dbReaderResources.ObjectRels.FirstOrDefault();

                result.ResultState = ValidationController.ValidateChecklistModel(result.Result, globals, nameof(ChecklistModel.WorkingListToResource));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchUser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.WorkingList.GUID,
                        ID_RelationType = Checklist.Config.LocalData.ClassRel_Working_Lists_belongs_to_user.ID_RelationType,
                        ID_Parent_Other = Checklist.Config.LocalData.ClassRel_Working_Lists_belongs_to_user.ID_Class_Right
                    }
                };

                var dbReaderUser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUser.GetDataObjectRel(searchUser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 ="Error while getting the User!";
                    return result;
                }

                result.Result.WorkingListToUser = dbReaderUser.ObjectRels.FirstOrDefault();

                result.ResultState = ValidationController.ValidateChecklistModel(result.Result, globals, nameof(ChecklistModel.WorkingListToUser));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReferences = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.WorkingList.GUID,
                        ID_RelationType = Checklist.Config.LocalData.ClassRel_Working_Lists_contains.ID_RelationType
                    }
                };

                var dbReaderReferences = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReferences.GetDataObjectRel(searchReferences);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the References!";
                    return result;
                }

                result.Result.WorkingListToReferences = dbReaderReferences.ObjectRels.Where(refItm => refItm.ID_Parent_Other != Checklist.Config.LocalData.Class_Logentry.GUID).ToList();

                result.ResultState = ValidationController.ValidateChecklistModel(result.Result, globals, nameof(ChecklistModel.WorkingListToReferences));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {
        }
    }
}
