﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using ProcessModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Validation
{
    public static class ValidationController
    {

        public static clsOntologyItem ValidateGetChecklistRequest(GetChecklistRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdChecklist))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdChecklist is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdChecklist))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdChecklist is no valid GUID!";
                return result;
            }

            return result;
        }
        public static clsOntologyItem ValidateChecklistModel(ChecklistModel checklistModel, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingList))
            {
                if (checklistModel.WorkingList == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Worklist found!";
                }

                if (checklistModel.WorkingList.GUID_Parent != Checklist.Config.LocalData.Class_Working_Lists.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The requested Object is no Workinglist!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingListToLogState))
            {

            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingListToReportField))
            {
                if (checklistModel.WorkingListToReportField == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Report-Field is related!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingListToReport))
            {
                if (checklistModel.WorkingListToReport == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Report is related!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingListToResource))
            {
                
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingListToUser))
            {
                if (checklistModel.WorkingListToUser == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No User is related!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(checklistModel.WorkingListToReferences))
            {
                if (!checklistModel.WorkingListToReferences.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No References are related!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateChangeChecklistEntryStateRequest(ChangeChecklistEntryStateRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();


            if (!request.ChecklistEntryStateItems.All(entry => !string.IsNullOrEmpty(entry.IdChecklist) &&
                !string.IsNullOrEmpty(entry.IdLogState) &&
                !string.IsNullOrEmpty(entry.IdReference) &&
                !string.IsNullOrEmpty(entry.IdUser)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Not all Ids are valid!";
                return result;
            }

            return result;
        }
    }
}
