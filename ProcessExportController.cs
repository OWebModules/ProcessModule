﻿using HtmlEditorModule;
using HtmlEditorModule.Models;
using MediaViewerModule.Connectors;
using MediaViewerModule.Primitives;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ProcessModule.Models;
using ProcessModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule
{
    public class ProcessExportController : AppController
    {
        public async Task<ResultItem<ExportHtmlProcessResult>> ExportHtmlProcess(ExportHtmlProcessRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ExportHtmlProcessResult>>(async () =>
            {
                var result = new ResultItem<ExportHtmlProcessResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ExportHtmlProcessResult()
                };
                var resultProcessHierarchy = await ExportHtmlProcessLocal(request);
                result.ResultState = resultProcessHierarchy.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var htmlConnector = new HtmlEditorConnector(Globals);

                var getHtmlDocument = await htmlConnector.GetHtmlObjectByRef(resultProcessHierarchy.Result);
                result.ResultState = getHtmlDocument.Result;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the related HTML-Document!";
                    return result;
                }

                var saveContent = await htmlConnector.SaveHtmlContent(resultProcessHierarchy.Result.Additional1, getHtmlDocument.OItemHtmlItem, resultProcessHierarchy.Result);
                result.Result.HtmlObjectProcess = saveContent.HtmlDocument;

                
                return result;
            });

            return taskResult;
        }

        private async Task<ResultItem<clsOntologyItem>> ExportHtmlProcessLocal(ExportHtmlProcessRequest request)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(async () =>
           {
               var result = new ResultItem<clsOntologyItem>
               {
                   ResultState = Globals.LState_Success.Clone()
               };

               if (string.IsNullOrEmpty(request.IdProcess) || !Globals.is_GUID(request.IdProcess))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The requested Id is not valid!";
                   return result;
               }

               var htmlExportConnector = new ExportHtmlDocController(Globals);

               var serviceAgent = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get Processitem...");
               var getOItemResult = await serviceAgent.GetOItem(request.IdProcess, Globals.Type_Object);
               request.MessageOutput?.OutputInfo($"Found Processitem: {getOItemResult.Result.Name}");


               result.ResultState = getOItemResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the process-item!";
                   return result;
               }

               if (result.ResultState.GUID == Globals.LState_Nothing.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The requested process cannot be found!";
                   return result;
               }

               if (getOItemResult.Result.GUID_Parent != Config.LocalData.Class_Process.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The requested IdProcess is no id of a process!";
                   return result;
               }

               result.Result = getOItemResult.Result;

               var fullPath = getOItemResult.Result.Name;
               if (!request.ProcessFullPath.EndsWith(fullPath))
               {
                   fullPath = $"{request.ProcessFullPath}\\{fullPath}";
               }

               var encodedFullPath = htmlExportConnector.EncodeHTML(fullPath);

               request.MessageOutput?.OutputInfo($"Encoded fullpath: {encodedFullPath}");

               var level = (request.Level <= 6 ? request.Level : 6);

               request.MessageOutput?.OutputInfo($"Get Processheader...");
               var getHtmlHeadingRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeHeading, false) { OrderId = level };
               var getHtmlHeadingResult = await htmlExportConnector.GetHtmlTag(getHtmlHeadingRequest);

               result.ResultState = getHtmlHeadingResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.SbHtml.Append(getHtmlHeadingResult.Result);
               request.SbHtml.Append(encodedFullPath);
               getHtmlHeadingRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeHeading, true) { OrderId = level };
               getHtmlHeadingResult = await htmlExportConnector.GetHtmlTag(getHtmlHeadingRequest);

               result.ResultState = getHtmlHeadingResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.SbHtml.Append(getHtmlHeadingResult.Result);
               request.MessageOutput?.OutputInfo($"Added Processheader");

               request.SbHtml.AppendLine();


               request.MessageOutput?.OutputInfo($"Get Description...");
               var htmlDescResult = await GetHtmlDescription(new List<clsOntologyItem> { result.Result });

               result.ResultState = htmlDescResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (htmlDescResult.Result.Any())
               {
                   request.SbHtml.Append(htmlDescResult.Result.First().Additional1);
               }

               request.MessageOutput?.OutputInfo($"Added Description");

               request.MessageOutput?.OutputInfo($"Get Requirements...");
               var requirementsResult = await GetHtmlRequirements(new List<clsOntologyItem> { result.Result });

               result.ResultState = requirementsResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (requirementsResult.Result.Any() && !string.IsNullOrEmpty(requirementsResult.Result.First().Additional1))
               {
                   request.SbHtml.Append(requirementsResult.Result.First().Additional1);
               }

               request.MessageOutput?.OutputInfo($"Added Requirements");


               request.MessageOutput?.OutputInfo($"Get Images...");

               var imagesResult = await GetHtmlMedia(new List<clsOntologyItem> { result.Result }, MultimediaItemType.Image);

               result.ResultState = imagesResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }


               if (imagesResult.Result.Any() && !string.IsNullOrEmpty(imagesResult.Result.First().Additional1))
               {
                   request.SbHtml.Append(imagesResult.Result.First().Additional1);
               }

               request.MessageOutput?.OutputInfo($"Added Images");

               request.MessageOutput?.OutputInfo($"Get Medias...");
               var pdfResult = await GetHtmlMedia(new List<clsOntologyItem> { result.Result }, MultimediaItemType.PDF);

               result.ResultState = pdfResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }


               if (pdfResult.Result.Any() && !string.IsNullOrEmpty(pdfResult.Result.First().Additional1))
               {
                   request.SbHtml.Append(pdfResult.Result.First().Additional1);
               }

               request.MessageOutput?.OutputInfo($"Added Medias");


               request.MessageOutput?.OutputInfo($"Get Audios...");
               var audioResult = await GetHtmlMedia(new List<clsOntologyItem> { result.Result }, MultimediaItemType.Audio);

               result.ResultState = audioResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (audioResult.Result.Any() && !string.IsNullOrEmpty(audioResult.Result.First().Additional1))
               {
                   request.SbHtml.Append(audioResult.Result.First().Additional1);
               }
               request.MessageOutput?.OutputInfo($"Added Audios");


               request.MessageOutput?.OutputInfo($"Get Videos...");
               var videoResult = await GetHtmlMedia(new List<clsOntologyItem> { result.Result }, MultimediaItemType.Video);

               result.ResultState = videoResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (videoResult.Result.Any() && !string.IsNullOrEmpty(videoResult.Result.First().Additional1))
               {
                   request.SbHtml.Append(videoResult.Result.First().Additional1);
               }
               request.MessageOutput?.OutputInfo($"Added Videos...");


               request.MessageOutput?.OutputInfo($"Get Subprocesses...");
               var getSubProcessesResult = await serviceAgent.GetSubProcessesL1(result.Result);

               result.ResultState = getSubProcessesResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Found {getSubProcessesResult.Result.Count} Subprocesses");

               foreach (var subProcess in getSubProcessesResult.Result.OrderBy(proc => proc.OrderID).ThenBy(proc => proc.Name_Other))
               {
                   var subProcessRequest = new ExportHtmlProcessRequest(subProcess.ID_Other) { ProcessFullPath = fullPath, Level = level + 1, SbHtml = request.SbHtml, MessageOutput = request.MessageOutput };
                   var resultSubProcess = await ExportHtmlProcessLocal(subProcessRequest);

                   result.ResultState = resultSubProcess.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }

               result.Result.Additional1 = request.SbHtml.ToString();

               return result;
           });
            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetHtmlMedia(List<clsOntologyItem> processItems, MultimediaItemType multimediaType)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async () =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = processItems
                };

                var htmlExportController = new ExportHtmlDocController(Globals);

                var dictAttributes = new Dictionary<string, string>();
                dictAttributes.Add(ExportHtmlDocController.AttributeBorder.Name, "1");
                var htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTable, false) { Attributes = dictAttributes };
                var tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var tableInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTable, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var tableEnd = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableRow, false);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var rowInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableRow, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var rowEnd = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableCol, false);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var colInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableCol, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var colEnd = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeBold, false);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var boldInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeBold, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var boldEnd = tagResult.Result;

                var mediaViewerController = new MediaViewerConnector(Globals);
                var serviceAgent = new ServiceAgentElastic(Globals);

                var modelResult = await serviceAgent.GetProcessModuleModel();

                var MediasResult = await mediaViewerController.GetMediaListItems(processItems, 
                    modelResult.Result.MediaStorePath.Name, 
                    "../Resources/UserGroupRessources",
                    multimediaType);
                var MediasItems = MediasResult.MediaListItems.OrderBy(image => image.OrderId).ToList();
                result.ResultState = MediasResult.Result;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var sbMedias = new StringBuilder();

                var returnStringBuilder = false;
                sbMedias.AppendLine(tableInit);

                foreach (var proc in processItems)
                {
                    proc.Additional1 = string.Empty;
                    var procMedias = MediasItems.Where(mediaItem => mediaItem.IdRef == proc.GUID);

                    if (procMedias.Any())
                    {
                        foreach (var item in procMedias)
                        {
                            returnStringBuilder = true;
                            sbMedias.Append(rowInit);
                            sbMedias.Append(colInit);

                            var attributes = new Dictionary<string, string>();
                            attributes.Add(ExportHtmlDocController.AttributeSRC.Name, item.FileUrl);
                            var docType = ExportHtmlDocController.DocTypeImages;

                            if (multimediaType != MultimediaItemType.Image)
                            {
                                attributes.Add(ExportHtmlDocController.AttributeWIDHT.Name, "800");
                                attributes.Add(ExportHtmlDocController.AttributeHEIGHT.Name, "600");
                                docType = ExportHtmlDocController.DocTypePDFsMedia;
                            }
                            
                            htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeImages, false) { Attributes = attributes };
                            tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                            result.ResultState = tagResult.ResultState;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }
                            sbMedias.Append(tagResult.Result);
                            sbMedias.Append(colEnd);
                            sbMedias.Append(rowEnd);
                        }
                        
                    }

                    sbMedias.AppendLine(tableEnd);

                    if (returnStringBuilder)
                    {
                        proc.Additional1 = sbMedias.ToString();
                    }
                    
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetHtmlRequirements(List<clsOntologyItem> processItems)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async () =>
            {
                var serviceAgent = new ServiceAgentElastic(Globals);
                var htmlExportController = new ExportHtmlDocController(Globals);
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = processItems
                };

                var referenceResult = await serviceAgent.GetProcessRefeences(processItems);
                result.ResultState = referenceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                
                var dictAttributes = new Dictionary<string, string>();
                dictAttributes.Add(ExportHtmlDocController.AttributeBorder.Name, "1");
                var htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTable, false) { Attributes = dictAttributes };
                var tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var tableInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTable, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var tableEnd = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableRow, false);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var rowInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableRow, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var rowEnd = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableCol, false);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var colInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeTableCol, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var colEnd = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeBold, false);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var boldInit = tagResult.Result;

                htmlTagRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeBold, true);
                tagResult = await htmlExportController.GetHtmlTag(htmlTagRequest);
                result.ResultState = tagResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var boldEnd = tagResult.Result;

                foreach (var proc in processItems)
                {
                    proc.Additional1 = string.Empty;
                    var sbRequirements = new StringBuilder();
                    var references = referenceResult.Result.Where(refItm => refItm.ProcessOrProcessLog.GUID == proc.GUID).Select(refItm => refItm.References);
                    if (!references.Any()) continue;

                    sbRequirements.AppendLine(tableInit);
                    sbRequirements.Append(rowInit);
                    sbRequirements.Append($"{colInit}{boldInit}Type{boldEnd}{colEnd}");
                    sbRequirements.Append($"{colInit}{boldInit}Requirement{boldEnd}{colEnd}");
                    sbRequirements.Append(rowEnd);

                    var namedReferences = references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_Application.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Applications", RefType = string.Empty, reference = refItm }).ToList();

                    


                    namedReferences.AddRange(references.Where(refItm => refItm.ID_RelationType == Config.LocalData.RelationType_needed_Documentation.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Documentations", RefType = refItm.Name_Parent_Other, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_File.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Files", RefType = string.Empty, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_Folder.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Folders", RefType = string.Empty, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_Group.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Groups", RefType = string.Empty, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_Manual.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Manuals", RefType = string.Empty, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_Media.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Medias", RefType = string.Empty, reference = refItm }));
                    
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_responsibility.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Medias", RefType = string.Empty, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_Role.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Roles", RefType = string.Empty, reference = refItm }));
                    namedReferences.AddRange(references.Where(refItm => refItm.ID_Parent_Other == Config.LocalData.Class_user.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).Select(refItm => new { Type = "Users", RefType = string.Empty, reference = refItm }));

                    var neededItems = references.Where(refItm => refItm.ID_RelationType == Config.LocalData.RelationType_needs.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).ThenBy(refItm => refItm.Name_Other);

                    namedReferences.AddRange(from refItm in neededItems
                                             join namedRef in namedReferences on refItm.ID_Other equals namedRef.reference.ID_Other into namedRefs
                                             from namedRef in namedRefs.DefaultIfEmpty()
                                             where namedRef == null
                                             select new { Type = "Needed", RefType = refItm.Name_Parent_Other, reference = refItm });


                    var neededChilds = references.Where(refItm => refItm.ID_RelationType == Config.LocalData.RelationType_needs_Child.GUID).OrderBy(refItm => refItm.OrderID).ThenBy(refItm => refItm.Name_Other).ThenBy(refItm => refItm.Name_Other);

                    namedReferences.AddRange(from refItm in neededChilds
                                             join namedRef in namedReferences on refItm.ID_Other equals namedRef.reference.ID_Other into namedRefs
                                             from namedRef in namedRefs.DefaultIfEmpty()
                                             where namedRef == null
                                             select new { Type = "Needed Childs", RefType = refItm.Name_Parent_Other, reference = refItm });

                    foreach (var referenceItem in namedReferences)
                    {

                        sbRequirements.Append(rowInit);
                        sbRequirements.Append($"{colInit}{referenceItem.Type}{colEnd}");
                        var refType = !string.IsNullOrEmpty(referenceItem.RefType) ? $" ({referenceItem.RefType})" : string.Empty;
                        sbRequirements.Append($"{colInit}{referenceItem.reference.Name_Other}{refType}{colEnd}");
                        sbRequirements.Append(rowEnd);
                    }

                    sbRequirements.AppendLine(tableEnd);

                    proc.Additional1 = sbRequirements.ToString();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetHtmlDescription(List<clsOntologyItem> processItems)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
            {
                var serviceAgent = new ServiceAgentElastic(Globals);
                var htmlExportController = new ExportHtmlDocController(Globals);
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = processItems
                };

                
                var processDescriptionResult = await serviceAgent.GetProcessDescription(processItems);

                result.ResultState = processDescriptionResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                if (processDescriptionResult.Result.Any())
                {
                    foreach (var processItem in processItems)
                    {
                        var sbHtml = new StringBuilder();
                        var descriptions = processDescriptionResult.Result.Where(desc => desc.ID_Object == processItem.GUID).OrderBy(attr => attr.OrderID);
                        foreach (var description in descriptions)
                        {
                            var paragraphRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeParagraph, false);
                            var paragraph = await htmlExportController.GetHtmlTag(paragraphRequest);
                            result.ResultState = paragraph.ResultState;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }
                            sbHtml.Append(paragraph.Result);
                            var desc = htmlExportController.EncodeHTML(description.Val_String);
                            sbHtml.Append(desc);
                            paragraphRequest = new GetHtmlTagRequest(ExportHtmlDocController.DocTypeParagraph, true);
                            paragraph = await htmlExportController.GetHtmlTag(paragraphRequest);
                            result.ResultState = paragraph.ResultState;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }
                            sbHtml.Append(paragraph.Result);
                            sbHtml.AppendLine();
                        }

                        processItem.Additional1 = sbHtml.ToString();
                    }
                    
                }

                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultItem<List<KendoTreeNode>>> GetProcessTree(GetProcessTreeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<KendoTreeNode>>>(async() =>
            {
                var result = new ResultItem<List<KendoTreeNode>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<KendoTreeNode>()
                };



                return result;
            });

            return taskResult;
        }
        

        public ProcessExportController(Globals globals) : base(globals)
        {
        }
    }
}
