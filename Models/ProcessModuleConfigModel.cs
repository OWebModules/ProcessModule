﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ProcessModuleConfigModel
    {
        public clsOntologyItem BaseConfig { get; set; }
        public clsOntologyItem Language { get; set; }
        public clsOntologyItem MediaStorePath { get; set; }
    }
}
