﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class GetProcessTreeRequest
    {
        public string IdProcess { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public CancellationToken CancellationToken { get; private set; }

        public GetProcessTreeRequest(CancellationToken cancellationToken)
        {
            CancellationToken = cancellationToken;
        }
    }
}
