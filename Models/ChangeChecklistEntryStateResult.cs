﻿using LogModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ChangeChecklistEntryStateResult
    {
        public List<clsObjectRel> ChecklistsToLogEntries { get; set; } = new List<clsObjectRel>();
        public List<LogItem> LogItems { get; set; } = new List<LogItem>();
    }
}
