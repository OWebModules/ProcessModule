﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class GetChecklistRequest
    {
        public string IdChecklist { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetChecklistRequest(string idChecklist)
        {
            IdChecklist = idChecklist;
        }
    }
}
