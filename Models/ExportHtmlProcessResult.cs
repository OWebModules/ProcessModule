﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ExportHtmlProcessResult
    {
        public clsOntologyItem HtmlObjectProcess { get; set; }
    }
}
