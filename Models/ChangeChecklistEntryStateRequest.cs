﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ChangeChecklistEntryStateRequest
    {
        public List<ChecklistEntryStateItem> ChecklistEntryStateItems { get; set; } = new List<ChecklistEntryStateItem>();
        public IMessageOutput MessageOutput { get; set; }
    }

    public class ChecklistEntryStateItem
    {
        public string IdChecklist { get; private set; }
        public string IdReference { get; private set; }
        public string IdLogState { get; private set; }

        public string IdUser { get; private set; }


        public string Message { get; private set; }

        public ChecklistEntryStateItem(string idChecklist, string idReference, string idLogstate, string idUser, string message)
        {
            IdChecklist = idChecklist;
            IdReference = idReference;
            IdLogState = idLogstate;
            IdUser = idUser;
            Message = message;
        }
    }
}
