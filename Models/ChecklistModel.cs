﻿using LogModule.Models;
using OntologyClasses.BaseClasses;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ChecklistModel
    {
        public clsOntologyItem WorkingList { get; set; }
        public clsObjectRel WorkingListToLogState { get; set; }
        public clsObjectRel WorkingListToReportField { get; set; }

        public clsObjectRel WorkingListToReport { get; set; }

        public clsObjectRel WorkingListToUser { get; set; }

        public clsObjectRel WorkingListToResource { get; set; }

        public List<clsObjectRel> WorkingListToReferences { get; set; } = new List<clsObjectRel>();

        public List<LogItem> LogItems { get; set; } = new List<LogItem>();

        public List<AdditionalReportField> AdditionalReportFields { get; set; } = new List<AdditionalReportField>();
    }
}
