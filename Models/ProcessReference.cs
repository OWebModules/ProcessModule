﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ProcessReference
    {
        public clsOntologyItem ProcessOrProcessLog { get; set; }
        public clsObjectRel ProcessReferences { get; set; }
        public clsObjectRel References { get; set; }
    }
}
