﻿using LogModule.Models;
using OntologyClasses.BaseClasses;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class Checklist
    {
        public string IdChecklist { get; set; }
        public string NameChecklist { get; set; }

        public string IdLogstate { get; set; }

        public string NameLogstate { get; set; }

        public string IdReport { get; set; }

        public string NameReport { get; set; }

        public string NameIdChecklistColumn { get; set; }

        public string IdReportField { get; set; }

        public string NameReportField { get; set; }

        public string IdUser { get; set; }

        public string NameUser { get; set; }

        public List<LogItem> LogItems { get; set; } = new List<LogItem>();

        public string IdResource { get; set; }

        public string NameResource { get; set; }

        public List<clsObjectRel> RelatedItems { get; set; } = new List<clsObjectRel>();

        public List<AdditionalReportField> AdditionalReportFields { get; set; } = new List<AdditionalReportField>();


    }
}
