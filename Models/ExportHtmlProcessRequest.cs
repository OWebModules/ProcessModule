﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    public class ExportHtmlProcessRequest
    {
        public string IdProcess { get; set; }

        public string ProcessFullPath { get; set; }

        public long Level { get; set; } = 1;

        public StringBuilder SbHtml { get; set; } = new StringBuilder();

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public ExportHtmlProcessRequest(string idProcess)
        {
            IdProcess = idProcess;
        }
    }
}
