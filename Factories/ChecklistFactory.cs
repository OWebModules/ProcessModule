﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using ProcessModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Factories
{
    public class ChecklistFactory
    {
        private Globals globals;
        public async Task<ResultItem<Models.Checklist>> CreateChecklist(ChecklistModel model, Globals globals)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<Models.Checklist>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = new Models.Checklist
                {
                    IdChecklist = model.WorkingList?.GUID,
                    NameChecklist = model.WorkingList?.Name,
                    IdLogstate = model.WorkingListToLogState?.ID_Other,
                    NameLogstate = model.WorkingListToLogState?.Name_Other,
                    IdReport = model.WorkingListToReport.ID_Other,
                    NameReport = model.WorkingListToReport.Name_Other,
                    IdReportField = model.WorkingListToReportField.ID_Other,
                    NameReportField = model.WorkingListToReportField.Name_Other,
                    IdResource = model.WorkingListToResource?.ID_Other,
                    NameResource = model.WorkingListToResource?.Name_Other,
                    IdUser = model.WorkingListToUser?.ID_Other,
                    NameUser = model.WorkingListToUser?.Name_Other,
                    RelatedItems = model.WorkingListToReferences,
                    LogItems = model.LogItems,
                    AdditionalReportFields = model.AdditionalReportFields,
                    NameIdChecklistColumn = "GUID_" + ReportModule.Helper.EscapeHelper.EscapeStringForMsSQLName(Checklist.Config.LocalData.Class_Working_Lists.Name)
                };

                return result;
            });

            return taskResult;
        }

        public ChecklistFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
